INTRODUCTION
------------

The KM Suite project is a custom module built specifically for Knowledge
Marketing API connection.  The KM Suite is a top level module that adds domain
tracking and taxonomy tracking.  Other modules may leverage the structure of
this module (such as the kmoverlay or kmsubsription).

INSTALLATION
------------
1. Download the KMSuite Library from github: https://github.com/kherda/kmsuite
and place it in sites/all/libraries/kmsuite/.  You should end up with:
sites/all/libraries/kmsuite/class.ecncommunicator.php

2. Download the Libraries module to sites/all/modules/.
https://www.drupal.org/project/libraries

3. Goto admin/modules and install the libraries module and kmsuite modules.

SETTINGS
------------
General settings for the kmsuite module can be found here:
admin/config/services/knowledge-marketing

Note: In order to use this module, you will need to have an API key that
is given from the team at Knowledge Marketing.

If you enable the taxonomy tracking, a block will be created, and you will need
to place in the section you would like the terms to be located.
